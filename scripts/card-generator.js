const quizContainer = document.getElementById('card__quiz');
let wishBlockColor;

const fullQuestions = [
    {
        question: "Как тебя зовут?",
        placeholder: "Введи свое имя...",
    },
    {
        question: "Кто получатель открытки?",
        placeholder: "Введи имя получателя...",
    }
];
const shortQuestions = [
    {
        question: "Выбери цвет открытки: ",
        answers: {
            a: "Новогодний красный",
            b: "Хвойный зелёный",
            c: "Мандариновый оранжевый",
            d: "Волшебный синий",
        }
    }
];
const textAreaQuestions =[
    {
        question: "Напиши пожелание своему получателю:",
        placeholder: "Пиши тут..."
    }
];

function buildQuiz() {
    const output = [];
    const answers = [];

    fullQuestions.forEach(
        (currentQuestion) => {
            answers.push(
                `<div class="answer__input">
                    <input type="text" class="ai_style full-answer" placeholder="${currentQuestion.placeholder}">
                </div>`
            );
            output.push (
                `<div class="slide"><div class="question__container qc_style">
                    <div class="question">
                        <div class="question__header">
                            ${currentQuestion.question}
                        </div>
                    </div>
                        ${answers.pop()}
                </div></div>`
            );
        }
    );

    const shortAnswers = [];
    shortQuestions.forEach(
        (currentQuestion) => {
              for (let answerNum in currentQuestion.answers) {
                  shortAnswers.push(
                      `<div class="radio-block">
                            <input id="radio-${answerNum}" name="radio-group-1" class="snow-radio" type="radio" value="${answerNum}">
                            <label class="snow-label" for="radio-${answerNum}">${currentQuestion.answers[answerNum]}</label>
                      </div>`
                  );
              }

            output.push(
                `<div class="slide"><div class="question__container qc_style">
                    <div class="question">
                        <div class="question__header">
                            ${currentQuestion.question}
                        </div>
                    </div>
                    <form class="radio__container">
                        ${shortAnswers.join('')}
                    </form>
                </div></div>`
            );
        }
    );

    const textAreaAnswers = [];
    textAreaQuestions.forEach(
        (currentQuestion) => {
            textAreaAnswers.push(
                `<div class="answer__input">
                    <textarea class="ta_style wish-answer" placeholder="${currentQuestion.placeholder}"></textarea>
                </div>`
            );

            output.push (
                `<div class="slide"><div class="question__container qc_style">
                    <div class="question">
                        <div class="question__header">
                            ${currentQuestion.question}
                        </div>
                    </div>
                        ${textAreaAnswers.pop()}
                </div></div>`
            );
        }
    );
    quizContainer.innerHTML = output.join('');
}

buildQuiz();

const nextButton = document.getElementById('next');
const previousButton = document.getElementById('previous');
const submitButton = document.getElementById('submit');
const slides = document.querySelectorAll(".slide");
let currentSlide = 0;

function showSlide(slideNum) {
    slides[currentSlide].classList.remove('slide_active');
    slides[slideNum].classList.add('slide_active');
    currentSlide = slideNum;

    if (currentSlide === 0) {
        previousButton.style.display = 'none';
    }
    else {
        previousButton.style.display = 'inline-block';
    }
    if(currentSlide===slides.length-1) {
        nextButton.style.display = 'none';
        submitButton.style.display = 'inline-block';
    }
    else {
        nextButton.style.display = 'inline-block';
        submitButton.style.display = 'none';
    }
}

function showNextSlide() {
    showSlide(currentSlide + 1);
}

function showPreviousSlide() {
    showSlide(currentSlide - 1);
}

function getColor(colorCode) {
    switch (colorCode) {
        case "a":
            return ["#8B0000", "#650000"];
        case "b":
            return ["#18892F", "#176927"];
        case "c":
            return ["#DD910C", "#b47709"];
        case "d":
            return ["#39378B", "#2c2b6a"];
        default:
            return [    "#8B0000", "#650000"];
    }
}

function changeCardColor() {
    const userAnswers = quizContainer.querySelectorAll(".snow-radio");
    for (let i = 0; i < userAnswers.length; i++) {
        if (userAnswers[i].checked === true) {
            let ansId = userAnswers[i].id;
            var color = document.getElementById(ansId).value;
        }
    }
    let [cardColor, wishColor] = getColor(color);
    document.querySelector(".card").style.background = cardColor;
    wishBlockColor = wishColor;
}
function changeWishColor() {
    document.querySelector(".content__wish").style.background = wishBlockColor;
}

function showCard() {
    changeCardColor();

    const userAnswers = quizContainer.querySelectorAll(".full-answer");
    const userWish = quizContainer.querySelector(".wish-answer");
    const headerContent = document.querySelector(".card__header");
    const footerContent = document.querySelector(".card__footer");
    const cardContent = document.querySelector(".card__content");
    quizContainer.remove();
    $(".card__footer").empty();

    let wish = userWish.value;

    let toName = userAnswers[1].value;
    let fromName = userAnswers[0].value;
    if (toName === '') {
        toName = 'Аноним';
    }
    if (fromName === '') {
        fromName = 'Аноним';
    }

    let quizHeader = document.querySelector(".header__start");
    quizHeader.remove();
    const headerOutput = [];
    const contentOutput = [];
    const footerOutput = [];
    headerOutput.push(
    `<div class="ch__content">
        <div class="chc__first-row">
            <div class="chc__left-angle">
                <div class="la__first">
                    <img src="rsc/snowflake1.png" alt="" width="70px" height="60px">
                    <img src="rsc/snowflake1.png" alt="" width="50px" height="40px">
                </div>
                <div class="la__second">
                    <img src="rsc/snowflake1.png" alt="" width="50px" height="40px">
                </div>
            </div>
            <span class="header__name">${toName}!</span>
            <img src="rsc/stamp.png" alt="" width="81px" height="85px">
        </div>
        <div class="chc__second-row">
            <span class="header__congrats">Поздравляю тебя с Новым 2019 годом!</span>
        </div>
    </div>`
    );
    contentOutput.push(
        `<div class="content__wish-container wc_style">
            <div class="content__wish">${wish}</div>
        </div>`
    );
    footerOutput.push(
      `<div>
         <span>С наилучшими пожеланиями, ${fromName}</span>
       </div>`
    );

    footerContent.classList.add("cf_after");
    footerContent.innerHTML = footerOutput.join('');
    cardContent.innerHTML = contentOutput.join('');
    changeWishColor();
    headerContent.innerHTML = headerOutput.join('');
}

showSlide(0);

function addListeners() {
    nextButton.addEventListener('click', showNextSlide);
    previousButton.addEventListener('click', showPreviousSlide);
    submitButton.addEventListener('click', showCard);
}

addListeners();

