const CANDIES_AMOUNT = 10;
const CANDYDRIFT_AMOUNT = 15;
let winWidth = $(window).width();
let winHeight = $(window).height();

function createCandy() {
    let candyImg = document.createElement("img");
    candyImg.src = "rsc/candycane.png";
    candyImg.classList.add("candy");
    candyImg.style.width = "50px";
    candyImg.style.height = "50px";
    candyImg.style.opacity = "0.85";
    candyImg.style.position = 'absolute';
    return candyImg;
}

function putCandy() {
    var candyLeft = Math.floor(Math.random()* (winWidth - 10));
    let candy = createCandy();
    candy.style.left = `${candyLeft}px`;
    candy.style.top = '0px';
    document.getElementById("candybox").appendChild(candy);
}

function putCandies() {
    for (let i = 0; i < CANDIES_AMOUNT; i++) {
        putCandy();
    }
    setTimeout(() => putCandies(), 1500);
}

function isReadyToClean(cnt) {
    return cnt === CANDYDRIFT_AMOUNT;
}

function cleanCandydrift() {
    var candies = document.getElementsByClassName("candy");
    Array.prototype.forEach.call(candies, function (item) {
       if (item.style.top === `${winHeight}px`) {
           $(item).hide('slow', function() {
               $(item).remove();
           });
       }
    });
}

function candyFall() {
    let candiesCnt = 0;
    var candies = document.getElementsByClassName("candy");
    let timerId = setInterval(() => {
        Array.prototype.forEach.call(candies, function (item) {
            let currentTop = Number(item.style.top.split('px')[0]);
            currentTop += 5;
            if (currentTop >= winHeight) {
                item.style.top = `${winHeight}px`;
                candiesCnt++;
                if (isReadyToClean(candiesCnt)) {
                    cleanCandydrift();
                    candiesCnt = 0;
                }
            }
            else
                item.style.top = `${currentTop%winHeight}px`;
        });
    }, 50);
}

function main() {
    putCandies();
    candyFall();
}

main();